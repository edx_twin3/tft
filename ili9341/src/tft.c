/* ************************************************************************** */

#include "tft.h"

/** Descriptive File Name

  @Company
    edx-TwIn3

  @File Name
    tft.c

  @Summary
    Libreria para pantalla 2.4 tft.

  @Description
    Controlador ILI9341.
 */
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
static inline void PMP_Write(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin, uint8_t buff){
	GPIOx->BSRR = (buff != GPIO_PIN_SET) ? (uint32_t)GPIO_Pin << 16U : GPIO_Pin;
}

static inline void PMP_PORT_Write(uint8_t data){
uint32_t aux, tmp;
	CS_ON();
	WR_ON();
	aux = data & 0xF4;
	aux =( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 ); //swaping
	tmp = (aux << 1) & 0x10;
	aux &= 0x40;
	aux |= tmp;
	aux = aux << 8;
		GPIOG->ODR = aux;
		  aux = (data >> 2) & 0x06;
		  tmp = (data >> 1) & 0x30;
		  aux |= tmp;
		  tmp = tmp >> 1;
		  tmp =( (tmp & 0x0F)<<4 | (tmp & 0xF0)>>4 ); //swaping
		  aux &= 0xA6;
		  aux |= tmp;
		  aux = ( (aux & 0x0F)<<4 | (aux & 0xF0)>>4 );
		  aux = aux << 8;
		GPIOE->ODR = aux;
	aux = data << 14; //15
		GPIOD->ODR = aux;
	aux = data << 3; //4
		GPIOF->ODR = aux;
	WR_OFF();
	CS_OFF();
}

static inline void PMP2PORT(pmp_data *buff){
	WR_ON();
	PMP_Write(LCD_D7_GPIO_Port, LCD_D7_Pin, buff->d7);
	PMP_Write(LCD_D6_GPIO_Port, LCD_D6_Pin, buff->d6);
	PMP_Write(LCD_D5_GPIO_Port, LCD_D5_Pin, buff->d5);
	PMP_Write(LCD_D4_GPIO_Port, LCD_D4_Pin,	buff->d4);
	PMP_Write(LCD_D3_GPIO_Port, LCD_D3_Pin, buff->d3);
	PMP_Write(LCD_D2_GPIO_Port, LCD_D2_Pin, buff->d2);
	PMP_Write(LCD_D1_GPIO_Port, LCD_D1_Pin, buff->d1);
	PMP_Write(LCD_D0_GPIO_Port, LCD_D0_Pin, buff->d0);
	WR_OFF();
	CS_OFF();
}


static inline void DRV_PMP0_Write(uint8_t data){
	pmp_data buff;
	CS_ON();
	memcpy(&buff,&data, sizeof(uint8_t));
	PMP2PORT(&buff);
	CS_OFF();
}

static void tft_cmd(uint8_t data){
    RS_STATE(RS_CMD);
    DRV_PMP0_Write(data); }
static void tft_data(uint8_t data){
    RS_STATE(RS_DATA);
    DRV_PMP0_Write(data); }
static void reset(void) {
    size_t i;
    RST_STATE(RESET_OFF);
    HAL_Delay(150);
    RST_STATE(RESET_ON);
    tft_cmd(0x00);
    for(i=0; i<3; i++){
    tft_cmd(0x00);  }
    RST_STATE(RESET_OFF);
}
static void writeRegister8(uint8_t cmd, uint8_t data){
    tft_cmd(cmd);
    tft_data(data); }
static void delayMicroseconds(void){
    size_t c;
    for(c=0;c!=2;c++);
}
static void writeRegister32(uint8_t r, uint32_t d) {
  tft_cmd(r);
  delayMicroseconds();
  tft_data(d >> 24);
  delayMicroseconds();
  tft_data(d >> 16);
  delayMicroseconds();
  tft_data(d >> 8);
  delayMicroseconds();
  tft_data(d);
}
static void setAddrWindow(int x1, int y1, int x2, int y2) {
 uint32_t t;
    t = x1;
    t <<= 16;
    t |= x2;
    writeRegister32(ILI9341_COLADDRSET, t);  // HX8357D uses same registers!
    t = y1;
    t <<= 16;
    t |= y2;
    writeRegister32(ILI9341_PAGEADDRSET, t); // HX8357D uses same registers!   
}
#if 0
static void ada_Rotation(uint8_t x){
rotation = (x & 3);
  switch (rotation) {
  case 0:
  case 2:
    _width = WIDTH;
    _height = HEIGHT;
    break;
  case 1:
  case 3:
    _width = HEIGHT;
    _height = WIDTH;
    break;
  }
}

static void setRotation(uint8_t x){
 uint16_t t;
   ada_Rotation(x);
   switch (rotation) {
   case 2:
     t = ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR;
     break;
   case 3:
     t = ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR;
     break;
  case 0:
    t = ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR;
    break;
   case 1:
     t = ILI9341_MADCTL_MX | ILI9341_MADCTL_MY | ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR;
   break;
   }
   writeRegister8(ILI9341_MADCTL, t ); // MADCTL
   // For 9341, init default full-screen address window:
   setAddrWindow(0, 0, _width - 1, _height - 1); // CS_IDLE happens here
}
#endif
#if TEST_TFT
static void flood(uint16_t color, uint32_t len) {
  uint16_t blocks;
  uint8_t  i, hi = color >> 8,
              lo = color;


  tft_cmd(0x2C);
  // Write first pixel normally, decrement counter by 1
  tft_data(hi);
  tft_data(lo);
  len--;

  blocks = (uint16_t)(len / 64); // 64 pixels/block
  
    while(blocks--) {
      i = 16; // 64 pixels/block / 4 pixels/pass
      do {
  tft_data(hi);tft_data(lo);
  tft_data(hi);tft_data(lo);
  tft_data(hi);tft_data(lo);
  tft_data(hi);tft_data(lo);
      } while(--i);
    }
    for(i = (uint8_t)len & 63; i--; ){
  tft_data(hi);
  tft_data(lo); }
}
#endif
extern void tft_init(void){
    reset();
    HAL_Delay(200);
    writeRegister8(ILI9341_SOFTRESET, 0);
    HAL_Delay(50);
    writeRegister8(ILI9341_DISPLAYOFF, 0);

    writeRegister8(ILI9341_POWERCONTROL1, 0x23);
    writeRegister8(ILI9341_POWERCONTROL2, 0x10);
    tft_cmd(ILI9341_VCOMCONTROL1);
    tft_data(0x2B);
    tft_data(0x2B);
    writeRegister8(ILI9341_VCOMCONTROL2, 0xC0);
    writeRegister8(ILI9341_MEMCONTROL, ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
    writeRegister8(ILI9341_PIXELFORMAT, 0x55);
    tft_cmd(ILI9341_FRAMECONTROL);
    tft_data(0x00);
    tft_data(0x1B);
    writeRegister8(ILI9341_ENTRYMODE, 0x07);
    /* writeRegister32(ILI9341_DISPLAYFUNC, 0x0A822700);*/
    writeRegister8(ILI9341_SLEEPOUT, 0);
    HAL_Delay(150);
    writeRegister8(ILI9341_DISPLAYON, 0);
    HAL_Delay(500);
	// *** SPFD5408 change -- Begin
	// Not tested yet
	//writeRegister8(ILI9341_INVERTOFF, 0);
	//delay(500);
    // *** SPFD5408 change -- End
    setAddrWindow(0, 0, WIDTH-1, HEIGHT-1);
//    setRotation(LANDSCAPE);
#if TEST_TFT
    /*FILLSCREEN*/
    flood(TFTLCD_ORANGE, HEIGHT*WIDTH);
    while(1);
#else
    tft_cmd(ILI9341_MEMORYWRITE);
    RS_STATE(RS_DATA);    
#endif
}
extern void tft_imagen(uint8_t *imagen){
	uint32_t c;
    for(c=0;c!=IMA_BUFF_SIZE;c++){
    DRV_PMP0_Write(((uint8_t *)imagen)[c]);
    //PMP_PORT_Write(((uint8_t *)imagen)[c]);
    }
}   
/* *****************************************************************************
 End of File
 */
