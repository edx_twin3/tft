/* ************************************************************************** */
/** Descriptive File Name

  @Company
    edx-TwIn3

  @File Name
    tft.h

  @Summary
    Libreria para pantalla 2.4 tft.

  @Description
    Controlador ILI9341.
 */
/* ************************************************************************** */
#ifndef _TFT_H    /* Guard against multiple inclusion */
#define _TFT_H
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */
#include "main.h"
#include <stdbool.h>
#include <string.h>
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
    /* ************************************************************************** */
    /* ************************************************************************** */
    /* Section: Constants                                                         */
    /* ************************************************************************** */
    /* ************************************************************************** */
#define TEST_TFT            (false)
#define IMAGENES_GIF        (true)
//#define PMP_DMA             (true)


#define IMA_BUFF_SIZE       (2*(WIDTH*HEIGHT))
#define FRAMES      		(61) //mouse 191 hello 11 run 55 dragon 61 parasoul 32
//Data or CMD selection, low level: command, high level: data
#define RS_PIN              (LCD_RS_Pin)
#define RS_CMD              (GPIO_PIN_RESET)
#define RS_DATA             (GPIO_PIN_SET)
#define RS_STATE(x)         (HAL_GPIO_WritePin(LCD_RS_GPIO_Port, RS_PIN, x))
//Chip Reset signal active LOW
#define RST_PIN              (LCD_RST_Pin)
#define RESET_ON             (GPIO_PIN_RESET)
#define RESET_OFF            (GPIO_PIN_SET)
#define RST_STATE(x)         (HAL_GPIO_WritePin(LCD_RST_GPIO_Port, RST_PIN, x))
//Chip Select active on LOW
#define CS_PIN                (LCD_CS_Pin)
#define CS_ON()               (GPIOB->BSRR = (uint32_t)LCD_CS_Pin << 16U)
#define CS_OFF()              (GPIOB->BSRR = (CS_PIN))

//Write Data
#define WR_PIN				  (LCD_WR_Pin)
#define WR_ON()				  (GPIOC->BSRR = (uint32_t)WR_PIN << 16U)
#define WR_OFF()			  (GPIOC->BSRR = (WR_PIN))
//Read Data
#define RD_PIN				  (LCD_RD_Pin)
#define RD_ON()				  (GPIOA->BSRR = (uint32_t)LCD_RD_Pin << 16U)
#define RD_OFF()			  (GPIOA->BSRR = (RD_PIN))


#define WIDTH   (240)
#define HEIGHT  (320)    

#define PORTRAIT        (0)
#define LANDSCAPE       (3)
#define PORTRAIT_REV    (2) 
#define LANDSCAPE_REV   (1)


#define ILI9341_SOFTRESET          0x01
#define ILI9341_SLEEPIN            0x10
#define ILI9341_SLEEPOUT           0x11
#define ILI9341_NORMALDISP         0x13
#define ILI9341_INVERTOFF          0x20
#define ILI9341_INVERTON           0x21
#define ILI9341_GAMMASET           0x26
#define ILI9341_DISPLAYOFF         0x28
#define ILI9341_DISPLAYON          0x29
#define ILI9341_COLADDRSET         0x2A
#define ILI9341_PAGEADDRSET        0x2B
#define ILI9341_MEMORYWRITE        0x2C
#define ILI9341_PIXELFORMAT        0x3A
#define ILI9341_FRAMECONTROL       0xB1
#define ILI9341_DISPLAYFUNC        0xB6
#define ILI9341_ENTRYMODE          0xB7
#define ILI9341_POWERCONTROL1      0xC0
#define ILI9341_POWERCONTROL2      0xC1
#define ILI9341_VCOMCONTROL1      0xC5
#define ILI9341_VCOMCONTROL2      0xC7
#define ILI9341_MEMCONTROL      0x36
#define ILI9341_MADCTL  0x36

#define ILI9341_MADCTL_MY  0x80
#define ILI9341_MADCTL_MX  0x40
#define ILI9341_MADCTL_MV  0x20
#define ILI9341_MADCTL_ML  0x10
#define ILI9341_MADCTL_RGB 0x00
#define ILI9341_MADCTL_BGR 0x08
#define ILI9341_MADCTL_MH  0x04

// Color definitions
#define	TFTLCD_BLACK           0x0000
#define TFTLCD_WHITE           0xFFFF
#define	TFTLCD_BLUE            0x001F
#define	TFTLCD_RED             0xF800
#define	TFTLCD_GREEN           0x07E0
#define TFTLCD_CYAN            0x07FF
#define TFTLCD_MAGENTA         0xF81F
#define TFTLCD_YELLOW          0xFFE0
#define TFTLCD_ORANGE          0xFC80
#define TFTLCD_PURPLE          0x781F
#define TFTLCD_SIENNA          0xF811
#define TFTLCD_NEONYELLOW      0x9FE0
#define TFTLCD_EMERALD         0x05E5
#define TFTLCD_BLUE3           0x03FF
#define TFTLCD_ROYALBLUE       0x3333
#define TFTLCD_BROWN2          0x8888
#define TFTLCD_PINK            0xFADF
#define TFTLCD_DKPINK          0xFCDF
#define TFTLCD_BROWN1          0x7083
#define TFTLCD_OLIVE           0x6BC3
 
typedef union{
  struct{
    uint8_t d0:1;
    uint8_t d1:1;
    uint8_t d2:1;
    uint8_t d3:1;
    uint8_t d4:1;
    uint8_t d5:1;
    uint8_t d6:1;
    uint8_t d7:1;
  };
}pmp_data;

void tft_init(void);
void tft_imagen(uint8_t *imagen);
#ifdef __cplusplus
}
#endif

#endif /* _TFT_H */

/* *****************************************************************************
 End of File
 */
