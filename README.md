This is the initial version of the ILI9341 driver for stm32, maybe i will add more features later, so if you have something new sent a merge request :-)
This driver was test on a [NUCLEO-H753ZI](https://www.st.com/en/evaluation-tools/nucleo-h753zi.html) in theory it must work on any other dev board, indeed it does not need much effort to port it to any other mcu, the display was a 2.5'' tft screen,  [video showing that it works!](https://youtu.be/oezJ-fJOnRk)
As always any doubt, question or comment, i'll try to respond ASAP, you can reach me at [edx-Twin3](https://edx-twin3.org)
See ya :1
